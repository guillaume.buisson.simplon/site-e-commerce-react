import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import BasicRouter from "./components/Router";
import Navbar from "./components/littleComponent/Navbar";
import Breadcrumb from "./components/littleComponent/Breadcrumb";

export default function App() {

  return (
    <div className="App">
      <header className="App-header">
        <Navbar />
        <Breadcrumb/>
        <BasicRouter />
      </header>
    </div>
  );
}

