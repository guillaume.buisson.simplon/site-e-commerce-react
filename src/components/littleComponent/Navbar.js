import {useEffect, useState} from "react";
import Logo from "../../imgs/logoGifShop.gif";
import Avatar from "../../imgs/AccountAvatar.png";
import Cart from "../../imgs/cart.png";

export default function Navbar() {
    const [Username, setUsername] = useState('');

    useEffect(() => {
        let username = localStorage.getItem('Username')

        if(username) {
            setUsername(username);
        }
    }, [])

    return(
        <nav className="navbar navbar-light bg-light px-3 d-flex align-items-center">
            <div>
                <a className="navbar-brand" href="/home"><img className="myLogo" src={Logo} alt=""/><span>MyShop</span></a>
            </div>
            <div>
                <a href="/cart"><img className="myLogoAvatar mx-4" src={Cart} alt="Cart Logo"/></a>
                <img className="myLogoAvatar" src={Avatar} alt="Avatar Logo"/>
                <span className="mx-3 user-select-none">{Username}</span>
            </div>
        </nav>
    )
}