import {useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import ReactStars from "react-rating-stars-component/dist/react-stars";
import LoaderManager from "../littleComponent/Loader";


export default function CategorieController() {
    const { category } = useParams();
    const [allProducts, setallProducts] = useState([]);

    useEffect(() => {
        fetch(`https://fakestoreapi.com/products/category/${category}`)
            .then(res => res.json())
            .then(json => setallProducts(json))
            .catch(err=>console.log(err))
    }, [category, allProducts])

    const listProducts = allProducts.map((product) =>
            <div key={product.id} className="d-flex flex-column align-items-between justify-content-between myCards2 card m-3 p-5 shadow">
                <img className="card-img-top myImgMini" src={product.image} alt={product.title} />
                <div className="my-5 d-flex flex-column justify-content-center">
                    <h5 className="card-title" title={product.title}>{product.title.slice(0,20) + "..."}</h5>
                    <div className="d-flex align-items-center justify-content-center my-2 mt-1">
                        <ReactStars
                            count={5}
                            value={Math.round(product.rating.rate)}
                            size={36}
                            edit={false}
                            activeColor="red"
                        />
                        <span className="text-muted mx-2 user-select-none">{product.rating.count} avis</span>
                    </div>
                    <a href={"/home/" + encodeURI(product.category) + "/" + product.id} className="btn btn-secondary"> Voir le produit </a>
                </div>
            </div>
    )

    const createProductCard = (
        <div>
            <h1 className="text-center my-5 myFont">{category}</h1>
            <div className="d-flex flex-wrap justify-content-center">{listProducts}</div>
        </div>
    );

    return allProducts.length > 0 ? createProductCard : <LoaderManager visible={allProducts} />
}