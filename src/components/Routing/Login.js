import background from "../../imgs/backgroundShop2.gif";
import {useState} from "react";

export default function LoginController() {
    const [Username, setUsername] = useState('');

    const onSubmit = (e) => {
        e.preventDefault();
        localStorage.setItem('Username', Username);
        window.location.href = "/home";
        // fetch('https://fakestoreapi.com/auth/login',{
        //     method:'POST',
        //     body:JSON.stringify({
        //         "username": "johnd",
        //         "password": "m38rmF$"
        //     })
        // })
        //     .then(res=>res.json())
        //     .then(json=>console.log(json))
    }

    const onChange = (e) => {
        setUsername(e.target.value)
    }

    const redirectToRegister = () => {
        window.location.href = "/register";
    }

    return(
        <div className="vh-100 d-flex justify-content-start align-items-center">
            <div className="w-50 border-right">
                <h1 className="text-center display-1 myFont">Log In</h1>
                <form className="w-75 p-5 mx-auto shadow" onSubmit={onSubmit}>
                    <div className="form-group">
                        <label htmlFor="username">Username</label>
                        <input type="text" className="form-control" id="username" placeholder="Enter username" onChange={onChange} required/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="exampleInputPassword1">Password</label>
                        <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" />
                    </div>
                    <div className="form-check">
                        <input type="checkbox" className="form-check-input" id="exampleCheck1" />
                        <label className="form-check-label" htmlFor="exampleCheck1">Check me out</label>
                    </div>
                    <button type="submit" className="btn btn-success">Log In</button>
                </form>
            </div>
            <div className="w-50 d-flex flex-column align-items-center justify-content-center">
                <h1 className="text-center display-1 myFont">Register</h1>
                <img className="myBackgroundLogin shadow" draggable="false" src={background} alt="background" onClick={redirectToRegister}/>
            </div>
        </div>
    )
}