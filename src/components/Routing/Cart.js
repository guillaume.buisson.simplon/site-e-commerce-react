import {useEffect, useState} from "react";


export default function CartController() {
    const [Product, setProduct] = useState(null);
    const [totPrice, setTotPrice] = useState(0);
    let createCart = "";

    useEffect(() => {
        let myArray = [];
        myArray.push(JSON.parse(localStorage.getItem('product')))
        if(!Product) {
            setProduct(myArray);
            console.log(myArray)
            myArray[0].forEach(e => {
                let total = 0;
                console.log(totPrice, e.price)
                total = totPrice + e.price
                setTotPrice(total);
            })
        }
    }, [Product, totPrice]);

    if(Product) {
        createCart = Product.map((oneProduct) =>
            <div key={oneProduct} className="d-flex shadow justify-content-center align-items-center">
                <div className="p-5">
                    <img className="myImgMiniCart" src={oneProduct.image} alt=""/>
                </div>
                <div className="p-5 border-right">
                    <h3>{oneProduct.title}</h3>
                </div>
                <div className="p-5">
                    <p className="myFont">{oneProduct.price}€</p>
                </div>
            </div>
        )
    }

    const cart = Product ? (
        <div>
            {createCart}
            <div>
                {totPrice}
            </div>
        </div>
    ) : (
        <div>
            <p>Il n'y a pas encore d'articles... <a href="/home" className="myLink mx-3">Magasin</a></p>
        </div>
    )

    return cart;
}