
export default function RegisterController() {

    const onSubmit = (e) => {
        e.preventDefault();
        const username = document.getElementById("username");
        const password = document.getElementById("password");
        const email = document.getElementById("email");
        const nom = document.getElementById("nom");
        const prenom = document.getElementById("prenom");
        const address = document.getElementById("adresse");
        const phone = document.getElementById("phone");

        //remove after mock
        window.location.href = "/";

        fetch('https://fakestoreapi.com/users',{
            method:"POST",
            body:JSON.stringify(
                {
                    email: email.value,
                    username: username.value,
                    password: password.value,
                    name:{
                        firstname: prenom.value,
                        lastname: nom.value
                    },
                    address:{
                        city:'kilcoole',
                        street: address.value,
                        number:3,
                        zipcode:'12926-3874',
                        geolocation:{
                            lat:'-37.3159',
                            long:'81.1496'
                        }
                    },
                    phone: phone.value
                }
            )
        })
            .then(res=>res.json())
            .then(json=>console.log(json))
            .catch(err=>console.log(err))
    }

    return(
        <div className="d-flex flex-column align-items-center justify-content-center vh-100 p-5">
            <form className="w-100 p-5 mx-auto shadow" onSubmit={onSubmit}>
                <div className="d-flex">
                    <div className="w-50 p-2 px-4 border-right">
                        <div className="form-group">
                            <label htmlFor="username">Username*</label>
                            <input type="text" className="form-control" id="username" placeholder="Enter username"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="password">Password*</label>
                            <input type="password" className="form-control" id="password" placeholder="Enter Password"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="password">Email*</label>
                            <input type="email" className="form-control" id="email" placeholder="Enter Email"/>
                        </div>
                    </div>
                    <div className="w-50 p-2 px-4">
                        <div className="form-group">
                            <label htmlFor="nom">Nom</label>
                            <input type="text" className="form-control" id="nom" placeholder=""/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="prenom">Prénom</label>
                            <input type="text" className="form-control" id="prenom" placeholder=""/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="adresse">Adresse</label>
                            <input type="text" className="form-control" id="adresse" placeholder=""/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="phone">Téléphone</label>
                            <input type="phone" className="form-control" id="phone" placeholder=""/>
                        </div>
                    </div>
                </div>
                <button className="btn btn-primary mt-4 mx-2">Register</button>
            </form>
        </div>
    )
}