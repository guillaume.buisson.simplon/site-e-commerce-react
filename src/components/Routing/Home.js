import {useEffect, useState} from "react";
import LoaderManager from "../littleComponent/Loader";


export default function HomeController() {
    const [allCat, setallCat] = useState([]);
    const [Username, setUsername] = useState('');

    useEffect(() => {
        setUsername(localStorage.getItem('Username'));

        fetch('https://fakestoreapi.com/products/categories')
            .then(res=>res.json())
            .then(json=>setallCat(json))
    }, [allCat])

    const listCat = allCat.map((cat) =>
        <a key={cat} href={"/home/" + encodeURI(cat)} className="myCards m-2">
            <div className="card shadow">
                <div className="card-body">
                    <h5 className="card-title">{cat}</h5>
                </div>
            </div>
        </a>
    )

    const createCatCard = Username ? (
        <div>
            <h1 className="text-center my-5 myFont">Catégories</h1>
            <div className="d-flex flex-wrap mx-5 myFont">{listCat}</div>
        </div>
    ) : (
        <div className="d-flex align-items-center justify-content-center w-100 hv-100">
            <a href="/login" className="btn btn-success">LogIn</a>
        </div>
    )

    return allCat.length > 0 ? createCatCard : <LoaderManager visible={allCat}/>
}