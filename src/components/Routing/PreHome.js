import background from "../../imgs/backgroundShop.gif";

export default function PreHomeController() {

    const redirectToLogin = () => {
        window.location.href = "/login";
    }

    const redirectToHome = () => {
        window.location.href = "/home";
    }


    return(
        <div className="w-100 min-vh-100 d-flex">
            <img className="myBackground shadow w-100" draggable="false" src={background} alt="background"/>
            <div className="d-flex flex-column justify-content-center align-items-end">
                <div className="d-flex flex-column justify-content-center align-items-center whiteBackgroundOpacity p-3 w-50">
                    <h2>Description</h2>
                    <p className="mx-3">Et quoniam mirari posse quosdam peregrinos existimo haec lecturos forsitan, si contigerit, quamobrem cum oratio ad ea monstranda deflexerit quae Romae gererentur, nihil praeter seditiones narratur et tabernas et vilitates harum similis alias, summatim causas perstringam nusquam a veritate sponte propria digressurus. Omitto iuris dictionem in libera civitate contra leges senatusque consulta; caedes relinquo; libidines praetereo, quarum acerbissimum extat indicium et ad insignem memoriam turpitudinis et paene ad iustum odium imperii nostri, quod constat nobilissimas virgines se in puteos abiecisse et morte voluntaria necessariam turpitudinem depulisse. Nec haec idcirco omitto, quod non gravissima sint, sed quia nunc sine teste dico.</p>
                    <div className="d-flex">
                        <button onClick={redirectToLogin} className="btn btn-success mx-2">Log In</button>
                        <button onClick={redirectToHome} className="btn btn-primary mx-2">Site</button>
                    </div>
                </div>
            </div>
        </div>
    )
}