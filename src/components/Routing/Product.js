import {useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import ReactStars from "react-rating-stars-component";
import LoaderManager from "../littleComponent/Loader";


export default function ProductController() {
    const { id } = useParams();
    const [Product, setProduct] = useState(null);

    useEffect(() => {
        fetch(`https://fakestoreapi.com/products/${id}`)
            .then(res => res.json())
            .then(json => setProduct(json))
            .catch(err=>console.log(err))

    }, [id, Product])

    const onBuy = () => {
        let myProducts = [JSON.parse(localStorage.getItem('product'))];
        myProducts.push(Product);
        localStorage.setItem('product', JSON.stringify(myProducts));
        alert('Element Ajouté');
    }

    const createProduct = Product ? (
        <div>
            <div className="text-center d-flex p-5">
                <div className="w-50 shadow p-5">
                    <img className="myImg" src={Product.image} alt={Product.title}/>
                    <h3 className="mt-5">{Product.title}</h3>
                </div>
                <div className="w-50 myFont d-flex flex-column justify-content-center align-items-end">
                    <div className="border shadow p-5 w-50">
                        <p className="display-5">{Product.price + "€"}</p>
                        <button className="btn btn-success" onClick={onBuy}>Acheter</button>
                    </div>
                </div>
            </div>
            <div>
                <div className="border py-3">
                    <h2 className="myFont mx-5">Description</h2>
                </div>
                <div className="d-flex justify-content-between align-items-center px-5">
                    <div className="border shadow p-5 w-25">
                        <ReactStars
                            id="starManager"
                            count={5}
                            value={Math.round(Product.rating.rate)}
                            size={28}
                            edit={false}
                            activeColor="red"
                        />
                        <span id="avisManager" className="text-muted mx-2 user-select-none">{Product.rating ? Product.rating.count : 0} avis</span>
                    </div>
                    <div className="shadow w-50 p-5 my-5">
                        {Product.description}
                    </div>
                </div>
            </div>
        </div>
    ) : null;

    return Product ? createProduct : <LoaderManager visible={true} />
}