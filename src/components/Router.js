import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import HomeController from "./Routing/Home";
import CategorieController from "./Routing/Categorie";
import ProductController from "./Routing/Product";
import PreHomeController from "./Routing/PreHome";
import LoginController from "./Routing/Login";
import RegisterController from "./Routing/Register";
import CartController from "./Routing/Cart";

export default function BasicRouter() {
    return (
        <Router>
            <div>
                <Switch>
                    <Route exact path="/">
                        <PreHome />
                    </Route>
                    <Route exact path="/home">
                        <Home />
                    </Route>
                    <Route exact path="/home/:category">
                        <Categorie />
                    </Route>
                    <Route exact path="/home/:category/:id">
                        <Product />
                    </Route>
                    <Route exact path="/cart">
                        <Cart />
                    </Route>
                    <Route path="/login">
                        <Login />
                    </Route>
                    <Route path="/register">
                        <Register />
                    </Route>
                </Switch>
            </div>
        </Router>
    );
}

function PreHome() {
    return(
        <PreHomeController />
    )
}

function Home() {
    return (
        <HomeController />
    );
}

function Categorie() {
    return (
        <CategorieController />
    );
}

function Product() {
    return (
        <ProductController />
    );
}

function Cart() {
    return (
        <CartController />
    );
}

function Login() {
    return (
        <LoginController />
    );
}

function Register() {
    return (
        <RegisterController />
    );
}
